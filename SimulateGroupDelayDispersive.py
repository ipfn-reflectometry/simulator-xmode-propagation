import matplotlib.pylab as plt
import argparse
import numpy as np
import math

from scipy import constants as konst
from scipy import interpolate


import matplotlib.colors as colors

#from calc_invertProfile import calc_invertProfile


Fs = 200e6


def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    return (np.abs(np.array(array)-value)).argmin()


def calc_fpe(ne):
    """ Calculates the plasma frequency from the provided density
    """
    k1a = 4*np.power(konst.pi,2) * konst.epsilon_0  / np.power(konst.e,2)
    fpe = np.sqrt(ne/(k1a * konst.m_e))
    return fpe


def calc_bfield(fce):
    """ Calculates the magnetic field from a given electron cyclotron frequency
    """
    return (fce*2*konst.pi*konst.m_e)/(konst.e)

def calc_fce(bfield):
    """ Calculates the electron cyclotron frequency from a given Magnetic Field
    """
    return np.abs(konst.e*bfield/(2*konst.pi*konst.m_e))


def calc_invertProfile(fc,dg,los_ra,los_b,dvacuum=0,fce0=None):
    """Receives a probing frequency and group delay vector, and the radius
    and magnetic field profile and calculates the respective density profile
    of the X-mode upper cutoff

    Args:
        fc: cutoff frequency of each source vector
        dg: group delay of each plasma reflection
        los_ra: line of sight magnetic field radial position
        los_b: line of sight magnetic field
        dvacuum: calculated vacuum distance to be added at the end (default 0)

    Returns:
        re: radial position of the density profile starting at antenna origin
        ne: density profile measurement

    """
    dg = np.copy(dg)
    fc = np.copy(fc)
    bs = np.copy(los_b)
    ra = np.copy(los_ra)

    nbs = len(bs)
    ntao = len(dg)

    # Estimates the FF from the probing frequency and the density at the
    # first reflection
    ff = fc[0]
    #ff = fce0

    # Normalizing plasma distance to the ff position
    rp = ra-dvacuum # rp is the distance starting at the first plasma layer
    tao = dg
    tao = dg/2


    invertlc = False
    # Constant calculated apriori
    k1a = 4*np.power(konst.pi,2) * konst.epsilon_0  / np.power(konst.e,2)

    # Initialization for profile inversion

    M = np.zeros((ntao,ntao))
    A = np.zeros((ntao,ntao))
    N = np.ones((ntao,ntao))

    fce = np.zeros(ntao)
    fpe = np.zeros(ntao)
    phii = np.zeros(ntao)
    phi_delta = np.zeros(ntao)

    br = np.zeros(ntao-0)

    # Outputs
    ne = np.zeros(ntao-0)
    re = np.zeros(ntao-0)

    # The cyclotronic frequen
    ne[0] = 0
    re[0] = 0
    br[0] = np.interp(0,rp,bs)

    # B field interpolation function
    fbi = interpolate.interp1d(rp,bs,bounds_error=False,fill_value=bs[0]);

    # Cyclotronic frequency is at the origin or start of the plasma
    if fce0 is None:
        fce[0] = calc_fce(fbi(0))
    else:
        fce[0] = fce0
    # Plasma frequency is non zero as the density is non zero
    fpe[0] = np.sqrt(np.power(fc[0],2)-fc[0]*fce[0])

    #print 'fce0',fce0,fce[0]

    # Initialization of phii
    # Assume fc[j]-fc[j-1] is constant!
    delta_fc = fc[1]-fc[0];

    #taodiff = np.append([0],np.diff(tao))
    #tao2 = np.cumsum(np.abs(taodiff))

    # Constructs the phase array
    # phi[0] is zero
    for i in range(0,ntao):
        phii[i] = 2*konst.pi*(tao[0])/2*(fc[1]-fc[0])
        for j in range(1,i+1):
            #if invertlc ==0:
                #phii[i] = phii[i] + 2*konst.pi*(tao[j]+tao[j-1])/2*(fc[j]-fc[j-1])
                phii[i] = phii[i] + 2*konst.pi*(tao[j])*(fc[j]-fc[j-1])
                #phii[i] = phii[i] + 2*konst.pi*(tao[j-1]+tao[j])/2*(fc[j]-fc[j-1])
            #else:
            #    self.phii[i] = self.phii[i] + 2*konst.pi*tao[j]*(delta_fc);
        phi_delta[i] = phii[i] - phii[i-1]

    # Calculation of the radial distance for each density/probing frequency
    for i in range(1,ntao-0):
        fc_prev = fc[i]
        # Compute R...
        #fci = fc[i+1]
        fci = fc[i]
        if i%100: print 'Inverting Profile \t %0.0f %%'%(float(i)/ntao*100),'\r',
        for j in range(1,i+1):
            # Calculates the refractive index
            #print i,j,ntao

            #if j == 1:
            #    N[i,j-1] = calc_irx (fci,fpe[j-1],fce[j-1])
            #if j != i:
            #    N[i][j] = calc_irx (fci,fpe[j],fce[j])


            N[i][j] = calc_irx (fc[i],fpe[j],fce[j])

            #N[i][i]=0

            # Considers the previous refractive index instead of the average

            #A[i,j] = (2*konst.pi * fc[i] / konst.c) * (N[i,j] + N[i,j-1])/2
            #A[i,j] = (2*konst.pi * fci / konst.c) * N[i,j-1]*2
            #A[i,j] = (2*konst.pi * fc[i] / konst.c) * (N[i,j]*1.999 + 0.001*N[i,j-1])/2

            A[i,j] = (2*konst.pi * fc[i] / konst.c) * N[i,j]

            #if i <5:
            #    print i,j,'N',N[i][j],'A',A[i][j]
        #print i,A
        # Calculates auxiliary matrix M
        M[i,i] = A[i,i]
        for j in range(0,i):
        #    M[i,j] = A[i,j+1]-A[i,j]
        #for j in range(1,i):
            M[i,j] = A[i,j]-A[i,j+1]



        # Go to next i if M[i,i] is zero to avoid nan
        #print 'Mii',M[i,i]
        if M[i,i] == 0:
            print i,j,'SKIPPING OMG'
            continue

        # Compute Ri
        R = phii[i]
        for j in range(0,i+1):
            R = R +  M[i,j]*re[j]
        R = R/M[i,i]

        # Compute Ri
        R = phii[i]
        for j in range(0,i):
            R = R +  (A[i,j+1]-A[i,j])*re[j]
        R = R/A[i,i]



        if R <=0.0:
            R = 0.0
        # // end of Compute R
        re[i] = R
        #print 're',i,j,'=',R,M[i,i]#,phii[:i]
        #print 're',i,R

        # Estimates the new magnetic field from the new radial position
        # Should never happen
        assert not (re[i] < rp[0]), 're <0 %d %f'%(i,re[i])
        if re[i] < rp[0]:
            re[i]=rp[0]
        #br[i]=fbi(re[i])

        #print 'B fbi',br[i],rp -ra
        # Substituted fbi with direct numpy interp, faster processing?
        br[i] = np.interp(re[i],rp,bs)
        #print 'B np',br[i]
        # Calculate new fce
        fce[i] = calc_fce(br[i])

        # fc must always be above fce by definition
        if fc[i] <fce[i]:
            fc[i] = fce[i]
        assert fc[i]>=fce[i], 'fc %f must be above fce %f'%(fc[i],fce[i])


        if not invertlc:
            ne[i] = k1a * konst.m_e* fc[i]*(fc[i] - fce[i])
        else:
            ne[i] = k1a * konst.m_e* fc[i]*(fc[i] + fce[i])


        assert not ne[i] <0, 'ne <0 %d %f, r %f fc %f fce %f fc-fce %f'%(i,ne[i],re[i],fc[i], fce[i],fc[i]-fce[i])
        #if ne[i] == 0:
        #    ne[i] = ne[i-1]
        fpe[i] = np.sqrt(ne[i]/(k1a * konst.m_e))

        assert not np.isnan(fpe[i]), 'FPE IS NAN!!! %d  , %f %f %f '%(i,fpe[i],ne[i],k1a * konst.m_e)
    # Adds the vacuum distance to the measured R
    #if removedvacuum:
    #    self.re =  (dvacuum+self.re)
    re = re+dvacuum
    ra = re

    #print 'raff=',dvacuum, 'min diffne=',min(np.diff(self.ne)),#self.ne
    # Skips the first point in processed data as it is erroneous in calculation...
    skipidx = 1
    dp_raff = dvacuum
    #print 're',re
    print ''
    return re,ne,br

def calc_irx(f,fpe,fce):
    epsilon =  0#-1e-12
    norm = 1
    f = f/norm
    fpe = fpe/norm
    fce = fce/norm

    fpe2 = np.power(fpe,2)
    fce2 = np.power(fce,2)
    f2 = np.power(f,2)

    norm = 1
    #print norm
    f2 = f2/norm
    fce2 = fce2/norm
    fpe2 = fpe2/norm

    num = fpe2 * (1.0-fpe2/f2)
    denom = f2 - fpe2 - fce2
    #print num,denom
    out = 1.0 - num/denom

    #print fpe,out
    if out >= epsilon and out < 0.0:
        out = 0.0
    if out >= 0.0:
        return np.sqrt(out)
    else:
        #print 'irx out',out
        #print 'num',num,'denom',denom,f,fpe,fce
        #print 'calc_irx: Condition not met, returning 0',out
        return np.sqrt(out)
        pass
    return 0

def calc_irx_omode(f,fce):
    epsilon =  0#-1e-12
    norm = 1
    f = f/norm
    fpe = fpe/norm
    fce = fce/norm

    fpe2 = np.power(fpe,2)
    fce2 = np.power(fce,2)
    f2 = np.power(f,2)

    norm = 1
    #print norm
    f2 = f2/norm
    fce2 = fce2/norm
    fpe2 = fpe2/norm

    num = fpe2 * (1.0-fpe2/f2)
    denom = f2 - fpe2 - fce2
    #print num,denom
    out = 1.0 - num/denom

    #print fpe,out
    if out >= epsilon and out < 0.0:
        out = 0.0
    if out >= 0.0:
        return np.sqrt(out)
    else:
        #print 'irx out',out
        #print 'num',num,'denom',denom,f,fpe,fce
        #print 'calc_irx: Condition not met, returning 0',out
        return np.sqrt(out)
        pass
    return 0


def createRefractiveMatrix(r, ne, b, fc):
    """ Receives radial density and magnetic field profile and the cut off
    frequencies and builds the respective MxN refractive index matrix
        M = len(r)
        N = len(fc)

    Args:

    """

    M = len(r)
    N = len(fc)

    Nref = np.ones((N,M))*np.nan
    A = np.ones((N,M))#*np.nan
    Marray = np.zeros((N,M))#*np.nan

    # Cyclotronic frequency is at the origin or start of the plasma
    fce = calc_fce(b)
    # plasma frequency
    fpe = calc_fpe(ne)


    limitm = []
    limitn = []
    for n in range(N):
        if n%1000: print 'Calculating refractive matrix \t %0.0f %%'%(float(n)/N*100),'\r',

        allzero = False
        for m in range(M):
            # Calculates the refractive index
            Nref[n,m] = calc_irx (fc[n],fpe[m],fce[m])
            if Nref[n,m] < 0 or np.isnan(Nref[n,m]) :
                ##break
                #allzero = True
            #if allzero:
                Nref[n,m] = 0
                #pass

            #if Nref[n,m] >= 0 or True:
                #norm = 1
                #A[n,m] = norm*(2*konst.pi * fc[n] / konst.c) * (
                                #Nref[n,m]*2 + 0*Nref[n,m-1])/2
                #A[n,m] = A[n,m] *norm
            #A[n,m] = (2*konst.pi * fc[n] / konst.c) * (Nref[n,m])
                            #Nref[n,m]*2 + 0*Nref[n,m-1])/2
        #for m in range(0,n):
        #    Marray[n,m] = A[n,m]-A[n,m+1]
        #Marray[n,n] = A[n,n]

        #limitm.append(m)
        #limitn.append(n)
    print ''
    return Nref#, A,Marray,limitn,limitm


def calc_phase(r,fc,N):

    a,b = N.shape

    phase = np.zeros(a)
    for i in range(a):
        for j in range(1,b):
            if N[i,j]<=0:
                break
            # numeric integral
            phase[i] = phase[i] + (r[j]-r[j-1])*(N[i,j]+N[i,j-1])/2

        phase[i] = fc[i]*phase[i]*2*2*konst.pi/konst.c #- konst.pi/2

    return phase

def calc_delay(fc,phase):
    phasediff = np.gradient(phase)
    fcdiff = np.gradient(2*konst.pi*fc)
    delay = phasediff/fcdiff
    return delay

def create_density(radius,n0=5e19,m=2,n=5,Rgeo=1.65,dvac=0):
    """ Creates a typical density profile for the provided radius"""

    hfs_edge = 1.15+dvac
    lfs_edge = 2.15-dvac

    radius = radius
    #Plasma frequency array
    dens = []

    #for r in range(len(radius)):
    for r in radius:

        if (r <= Rgeo):

            dens.append( n0 * np.power(1.0 - np.power( (Rgeo - r)/(Rgeo - hfs_edge), m), n) )
        else:
            dens.append( n0 * np.power(1.0 - np.power( (r - Rgeo)/(lfs_edge - Rgeo), m), n) )

    dens = np.array(dens)
    dens[np.where(dens<0)]=0
    return np.array(dens)

def create_bprofile(radius,b0=2.5,Rgeo=1.65):
    bprofile = b0/(radius)*Rgeo
    return bprofile


def estimate_ff(fc,delay):
    """ estimates the ff frequency at the biggest delay jump """

    ffidx = np.argmax(np.diff(delay))+1
    ff = fc[ffidx]
    return ff

def estimate_fce0(fc,delay):
    """ estimates the fce0 frequency at the lowest delay value
    Only for these simulated data """

    fce0idx = np.argmin(delay)
    fce0 = fc[fce0idx]
    return fce0

def estimate_residualfce0(fc,ne_ff):
    """ estimates the fce0 frequency at the lowest delay value
    Only for these simulated data """
    fpe2 = np.power(calc_fpe(ne_ff),2)
    fce0 = fc-fpe2/fc
    return fce0



def estimate_vacuumDistance(los_ra,los_fce,fce0):
    """ Estimates the vacuum distance where the fce0 crosses the los_fce path"""

    dvac = np.interp(fce0,los_fce,los_ra)
    return dvac



def calc_ReflectionSignal(pf,delay,ff):
    """ Calculates the reflection signal from the group delay starting at ff"""

    ffidx = closest_index(pf,ff)

    dg = delay[ffidx:]
    fc = pf[ffidx:]
    return fc,dg

def calc_losBprofile(radius,bprofile,ff):
    """ Cuts the bprofile at the ff cyclotronic frequency """

    ffidx = closest_index(calc_fce(bprofile),ff)

    los_b = bprofile[ffidx:]
    los_ra = radius[:len(los_b)]

    return los_ra,los_b


def calc_delayVacuum(ra):
    """ Calculates the vacuum propagation delay for the provided positions"""
    dg = 2*ra/konst.c
    return dg

def create_ne_drop(radius,ne, r0,l0=0.005,depth=0.95):
    """ Creates a decrease in density at some point """

    r01idx = closest_index(radius,r0-l0/2)
    r02idx = closest_index(radius,r0+l0/2)
    r0idx = closest_index(radius,r0)

    neleft = ne[r01idx]
    neright = ne[r02idx]
    ner0 = ne[r0idx]*depth
    #ner0 = ne[r01idx:r02idx]*0.7


    ne[r01idx:r02idx] = ne[r01idx:r02idx]*depth

    rleft = radius[r01idx]
    rright = radius[r02idx]
    rr0 = radius[r0idx]

    nei = [ne[r01idx],ne[r0idx]*depth,ne[r02idx]]
    ri = [r[r01idx],r[r0idx],r[r02idx]]
    rinterval = radius[r01idx:r02idx]

    #gapne = interpolate.UnivariateSpline(ri, nei)(radius[r01idx:r02idx])
    #ne[r01idx:r02idx] = gapne

    #ne[r01idx:r02idx] = ne[r01idx:r02idx]*0.7
    #newne = interpolate.UnivariateSpline(radius, ne)(radius)

    z = np.polyfit(ri,nei, 2)
    f = np.poly1d(z)
    negap = f(rinterval)
    ne[r01idx:r02idx] = negap
    #newne = np.append(neleft,[ner0,neright])
    #print newne

    #newne = [neleft,ner0,neright].flatten()

    return ne

def create_ne_bump(radius,ne, r0,l0=0.005,depth=0.95):
    """ Creates a bump in density at some point """

    r01idx = closest_index(radius,r0-l0/2)
    r02idx = closest_index(radius,r0+l0/2)
    r0idx = closest_index(radius,r0)

    neleft = ne[r01idx]
    neright = ne[r02idx]
    ner0 = ne[r0idx]*depth
    #ner0 = ne[r01idx:r02idx]*0.7


    ne[r01idx:r02idx] = ne[r01idx:r02idx]*depth

    rleft = radius[r01idx]
    rright = radius[r02idx]
    rr0 = radius[r0idx]

    nei = [ne[r01idx],ne[r0idx]*depth,ne[r02idx]]
    ri = [r[r01idx],r[r0idx],r[r02idx]]
    rinterval = radius[r01idx:r02idx]

    #gapne = interpolate.UnivariateSpline(ri, nei)(radius[r01idx:r02idx])
    #ne[r01idx:r02idx] = gapne

    #ne[r01idx:r02idx] = ne[r01idx:r02idx]*0.7
    #newne = interpolate.UnivariateSpline(radius, ne)(radius)

    z = np.polyfit(ri,nei, 2)
    f = np.poly1d(z)
    negap = f(rinterval)
    ne[r01idx:r02idx] = negap
    #newne = np.append(neleft,[ner0,neright])
    #print newne

    #newne = [neleft,ner0,neright].flatten()

    return ne


def calc_error(x1,y1,x2,y2):
    """ Calculates the error between y1 and y2 for each point of y1"""

    newY2 = np.interp(x1,x2,y2)

    error = np.abs(newY2-y1)
    return error

if __name__ == '__main__':

    # Parses the arguments passed to the script
    parser = argparse.ArgumentParser(description='Processes the RICG raw data to provide the calculated density profiles.')
    #parser.add_argument('-s','--shotnumber',help='Shotnumber',required=False)
    #parser.add_argument('-n','--sweepnr',help='sweepnr',required=False,default=500)
    #parser.add_argument('-d','--dir',help='rawfile directory path',required=False,default=None)
    #parser.add_argument('-o','--output',help='Image output file',required=False,default=None)



    #args = parser.parse_args()

    #shotnumber = int(args.shotnumber)
    #sweepnr = int(args.sweepnr)
    #dir = args.dir
    #outputfile = args.output
    #saveoutput = True



    totalx = 600
    totalfc = 600

    b0=2.5
    Rant=2.15
    L = 0.1
    L = 0.05
    n0 = 10e19

    f1 = 40e9
    f2 = 100e9
    #f2 = 68e9
    f1 = 50e9

    if 0:
        f1 = 53.5e9
        f2 = 54.e9
        L = 0.005

    ne_ff = 10e16
    #f2 = 60e9
    radius = np.linspace(Rant, Rant-L, totalx)


    m=30
    n=3

    r0 = 0.0
    #ne = 1e19*step(r,r0)
    #ne = 5e19*ramp(r,r0)

    dvac = 0.002
    #dvac = 0.00
    ne = create_density(radius,n0=n0,m=m,n=n,dvac=dvac)


    #ne[100:120]=ne[80:100]

    bprofile = create_bprofile(radius,b0,Rgeo=1.65)

    radius = radius[0]-radius
    r = radius

    depth = 0.95
    ne = create_ne_drop(radius,ne,r0=0.02,l0=0.001,depth=depth)

#calc_invertProfile

    fc = np.linspace(f1,f2,totalfc)


    fce = fc
    b = calc_bfield(fce)
    bmin = calc_bfield(f1)
    b = np.linspace(bmin,bmin,totalx)


    b = bprofile
    N  = createRefractiveMatrix(r, ne, b, fc)

    phase = calc_phase(r,fc,N)



    delay = calc_delay(fc,phase)
    #print delay

    ff = estimate_ff(fc,delay)
    fce0 = estimate_fce0(fc,delay)
    fce0 = estimate_residualfce0(ff,ne_ff)

    los_ra = r
    los_b = bprofile

    estimated_dvac = estimate_vacuumDistance(los_ra,calc_fce(los_b),fce0)


    dp_fc,dp_dg = calc_ReflectionSignal(fc,delay,ff)

    newfc = np.linspace(dp_fc[0],dp_fc[-1],30)
    newdg = np.interp(newfc,dp_fc,dp_dg)
    #dp_fc = newfc
    #dp_dg = newdg

    dgvac = calc_delayVacuum(estimated_dvac)
    print 'Removing dvac from reflection, d=',estimated_dvac,'dgvac=',dgvac

    dp_dg = dp_dg-dgvac




    #los_ra,los_b = calc_losBprofile(r,bprofile,ff)
    #print len(los_ra),len(los_b)

    pr_re,pr_ne,pr_br = calc_invertProfile(dp_fc,dp_dg,los_ra,los_b,dvacuum=estimated_dvac,fce0=fce0)



    pr_dg = calc_delayVacuum(pr_re)
    #print pr_ne
    #fc = fc/1e9

    r_error = calc_error(pr_ne,pr_re,ne,r)
    ne_error = 1e19

    print 'Radial error. Mean:',r_error.mean(),'Std:',r_error.std()

    #print r_error


    fig,axarr = plt.subplots(2,2,sharex='col')#, sharex='col',sharey='row')

    #print tau

    colornorm = colors.LogNorm(vmin=0.003,vmax=1)
    colornorm = colors.Normalize(vmin=0,vmax=1)

    ax = axarr[0,0]

    ax.fill_betweenx(pr_ne/1e19, pr_re-r_error, pr_re+r_error,facecolor='red',alpha=0.1)
    #ax.fill_between(pr_re, (pr_ne-ne_error)/1e19, (pr_ne+ne_error)/1e19,facecolor='red',alpha=0.1)

    ax.plot(r,ne/1e19,label='Original',c='b',lw=2)
    ax.plot(pr_re,pr_ne/1e19,label='Calculated',c='r',lw=1)

    #ax.errorbar(pr_re, pr_ne/1e19, xerr=r_error)
    #ax.errorbar(pr_re, pr_ne/1e19, xerr=r_error,yerr=ne_error/1e19)


    ax.set_ylabel('Ne [1e19m-3]')

    ax.legend(fontsize='x-small',loc=2)

    ax.set_xlim([r.min(),r.max()])

    ylim_ne = [0,12]
    ax.set_ylim(ylim_ne)

    ax = axarr[1,0]
    ax.plot(r,bprofile,label='Original',c='b',lw=2)

    ax.plot(pr_re,pr_br,label='Calculated',c='r',lw=1)

    ax.set_ylabel('B [T]')
    ax.legend(fontsize='x-small',loc=4)

    ax.set_xlim([r.min(),r.max()])
    #ax.set_ylim(ylim_ne)
    ax.set_xlabel('Radius [m]')




    #axarr[1,0].pcolormesh(r,fc,N, cmap='Oranges',norm=colornorm)
    #axarr[1,0].plot(r[limitm],fc[limitn])

    #sym =0.00001
    #colornorm = colors.Normalize(vmin=-sym,vmax=sym)#np.max(A[np.where(not np.isnan(A))].flatten()))
    #cl = axarr[2,0].pcolormesh(r,fc,A)

    ax = axarr[0,1]
    ax.pcolormesh(fc/1e9,r,N.T, cmap='Oranges',norm=colornorm)
    ax.set_ylabel('Radius [m]')
    ax.set_title('X-mode dispersion',fontsize='x-small')
    ax.set_ylim([r.min(),r.max()])



    ax = axarr[1,1]
    ax.plot(fc/1e9,delay*1e9,c='b',lw=2,label='Delay of original')
    ax.plot(dp_fc/1e9,dp_dg*1e9,c='r',lw=1,label='Upper cutoff delay')
    ax.axvline(ff/1e9,c='c',label='First fringe reflection')

    ax.plot(dp_fc/1e9,pr_dg*1e9,c='g',label='Vacuum delay from profile')

    #ax.set_ylim([-0.1,4])
    ax.set_ylim([-0.1,np.max(delay*1e9)*1.2])
    ax.set_ylabel('Delay [ns]')
    ax.legend(fontsize='x-small')

    #ax = axarr[2,1]
    #ax.plot(fc/1e9,phase)
    ax.set_xlabel('Probing frequency [GHz]')

    plt.suptitle('Simulation of X-mode profile inversion')


    print 'Removing dvac from reflection, d=',estimated_dvac,'dgvac=',dgvac
    print 'fce0 ff',fce0,ff

    print 'fce(dvac), fce(estimateddvac)',calc_fce(np.interp(dvac,los_ra,los_b)),calc_fce(np.interp(estimated_dvac,los_ra,los_b))
    #plt.colorbar(cl)
    #strtitle = '#%s   %0.4f s   %d'%(shotnumber,time,sweepnr)
    #plt.suptitle(strtitle)

    plt.show()
