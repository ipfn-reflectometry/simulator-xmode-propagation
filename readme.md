X-mode group delay Simulator
===

This is a basic simulator of the X-mode propagation inside a provided electron density profile.

The (r,fc) mesh is mapped and the respective index of refraction N for each radial and cut off frequency point is calculated.

The corresponding group delays is measured from antenna position, through vacuum, and up to the cut off region where the N is <= zero. 
The calculated group delay is discretized and used to reconstruct a density profile.




Getting started
===


Installation and running the tool

```
# clone the simulator
git clone https://daguiam@gitlab.com/ipfn-reflectometry/simulator-xmode-propagation.git
# Executing program
cd simulator-xmode-propagation
python SimulateGroupDelayDispersive.py.py
```

![Profile gap](profile_with_gap.png)
